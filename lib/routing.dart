import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

class FadeThroughRoute<T> extends PageRouteBuilder<T> {
  final Widget widget;

  FadeThroughRoute({
    @required this.widget,
  }) : super(
          pageBuilder: (context, animation, secondaryAnimation) => widget,
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return FadeThroughTransition(
              animation: animation,
              secondaryAnimation: secondaryAnimation,
              child: child,
            );
          },
          transitionDuration: const Duration(milliseconds: 300),
        );
}

class SharedAxisRoute<T> extends PageRouteBuilder<T> {
  final Widget widget;

  SharedAxisRoute({
    @required this.widget,
  }) : super(
          pageBuilder: (context, animation, secondaryAnimation) => widget,
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            return SharedAxisTransition(
              transitionType: SharedAxisTransitionType.scaled,
              animation: animation,
              secondaryAnimation: secondaryAnimation,
              child: child,
            );
          },
          transitionDuration: const Duration(milliseconds: 300),
        );
}
