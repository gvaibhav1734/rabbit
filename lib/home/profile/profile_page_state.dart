import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:rednit/rednit.dart';

part 'profile_page_state.freezed.dart';

@freezed
abstract class ProfilePageState with _$ProfilePageState {
  const factory ProfilePageState.loading() = ProfilePageStateLoading;
  const factory ProfilePageState.signedOut() = ProfilePageStateSignedOut;
  const factory ProfilePageState.signingIn() = ProfilePageStateSigningIn;
  const factory ProfilePageState.signedIn() = ProfilePageStateSignedIn;
  const factory ProfilePageState.profile({
    @required User user,
  }) = ProfilePageStateProfile;
}
