extension ListSum<T extends num> on List<T> {
  T sum() => this.reduce((a, b) => a + b);
}