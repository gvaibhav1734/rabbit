// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'detailed_link_page_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$DetailedLinkPageStateTearOff {
  const _$DetailedLinkPageStateTearOff();

  DetailedLinkPageLoadingState loading({@required Link link}) {
    return DetailedLinkPageLoadingState(
      link: link,
    );
  }

  DetailedLinkPageSuccessState success(
      {@required Link link, @required List<Comment> comments}) {
    return DetailedLinkPageSuccessState(
      link: link,
      comments: comments,
    );
  }
}

// ignore: unused_element
const $DetailedLinkPageState = _$DetailedLinkPageStateTearOff();

mixin _$DetailedLinkPageState {
  Link get link;

  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(Link link),
    @required Result success(Link link, List<Comment> comments),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(Link link),
    Result success(Link link, List<Comment> comments),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(DetailedLinkPageLoadingState value),
    @required Result success(DetailedLinkPageSuccessState value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(DetailedLinkPageLoadingState value),
    Result success(DetailedLinkPageSuccessState value),
    @required Result orElse(),
  });

  $DetailedLinkPageStateCopyWith<DetailedLinkPageState> get copyWith;
}

abstract class $DetailedLinkPageStateCopyWith<$Res> {
  factory $DetailedLinkPageStateCopyWith(DetailedLinkPageState value,
          $Res Function(DetailedLinkPageState) then) =
      _$DetailedLinkPageStateCopyWithImpl<$Res>;
  $Res call({Link link});
}

class _$DetailedLinkPageStateCopyWithImpl<$Res>
    implements $DetailedLinkPageStateCopyWith<$Res> {
  _$DetailedLinkPageStateCopyWithImpl(this._value, this._then);

  final DetailedLinkPageState _value;
  // ignore: unused_field
  final $Res Function(DetailedLinkPageState) _then;

  @override
  $Res call({
    Object link = freezed,
  }) {
    return _then(_value.copyWith(
      link: link == freezed ? _value.link : link as Link,
    ));
  }
}

abstract class $DetailedLinkPageLoadingStateCopyWith<$Res>
    implements $DetailedLinkPageStateCopyWith<$Res> {
  factory $DetailedLinkPageLoadingStateCopyWith(
          DetailedLinkPageLoadingState value,
          $Res Function(DetailedLinkPageLoadingState) then) =
      _$DetailedLinkPageLoadingStateCopyWithImpl<$Res>;
  @override
  $Res call({Link link});
}

class _$DetailedLinkPageLoadingStateCopyWithImpl<$Res>
    extends _$DetailedLinkPageStateCopyWithImpl<$Res>
    implements $DetailedLinkPageLoadingStateCopyWith<$Res> {
  _$DetailedLinkPageLoadingStateCopyWithImpl(
      DetailedLinkPageLoadingState _value,
      $Res Function(DetailedLinkPageLoadingState) _then)
      : super(_value, (v) => _then(v as DetailedLinkPageLoadingState));

  @override
  DetailedLinkPageLoadingState get _value =>
      super._value as DetailedLinkPageLoadingState;

  @override
  $Res call({
    Object link = freezed,
  }) {
    return _then(DetailedLinkPageLoadingState(
      link: link == freezed ? _value.link : link as Link,
    ));
  }
}

class _$DetailedLinkPageLoadingState implements DetailedLinkPageLoadingState {
  const _$DetailedLinkPageLoadingState({@required this.link})
      : assert(link != null);

  @override
  final Link link;

  @override
  String toString() {
    return 'DetailedLinkPageState.loading(link: $link)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is DetailedLinkPageLoadingState &&
            (identical(other.link, link) ||
                const DeepCollectionEquality().equals(other.link, link)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(link);

  @override
  $DetailedLinkPageLoadingStateCopyWith<DetailedLinkPageLoadingState>
      get copyWith => _$DetailedLinkPageLoadingStateCopyWithImpl<
          DetailedLinkPageLoadingState>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(Link link),
    @required Result success(Link link, List<Comment> comments),
  }) {
    assert(loading != null);
    assert(success != null);
    return loading(link);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(Link link),
    Result success(Link link, List<Comment> comments),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(link);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(DetailedLinkPageLoadingState value),
    @required Result success(DetailedLinkPageSuccessState value),
  }) {
    assert(loading != null);
    assert(success != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(DetailedLinkPageLoadingState value),
    Result success(DetailedLinkPageSuccessState value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class DetailedLinkPageLoadingState implements DetailedLinkPageState {
  const factory DetailedLinkPageLoadingState({@required Link link}) =
      _$DetailedLinkPageLoadingState;

  @override
  Link get link;
  @override
  $DetailedLinkPageLoadingStateCopyWith<DetailedLinkPageLoadingState>
      get copyWith;
}

abstract class $DetailedLinkPageSuccessStateCopyWith<$Res>
    implements $DetailedLinkPageStateCopyWith<$Res> {
  factory $DetailedLinkPageSuccessStateCopyWith(
          DetailedLinkPageSuccessState value,
          $Res Function(DetailedLinkPageSuccessState) then) =
      _$DetailedLinkPageSuccessStateCopyWithImpl<$Res>;
  @override
  $Res call({Link link, List<Comment> comments});
}

class _$DetailedLinkPageSuccessStateCopyWithImpl<$Res>
    extends _$DetailedLinkPageStateCopyWithImpl<$Res>
    implements $DetailedLinkPageSuccessStateCopyWith<$Res> {
  _$DetailedLinkPageSuccessStateCopyWithImpl(
      DetailedLinkPageSuccessState _value,
      $Res Function(DetailedLinkPageSuccessState) _then)
      : super(_value, (v) => _then(v as DetailedLinkPageSuccessState));

  @override
  DetailedLinkPageSuccessState get _value =>
      super._value as DetailedLinkPageSuccessState;

  @override
  $Res call({
    Object link = freezed,
    Object comments = freezed,
  }) {
    return _then(DetailedLinkPageSuccessState(
      link: link == freezed ? _value.link : link as Link,
      comments:
          comments == freezed ? _value.comments : comments as List<Comment>,
    ));
  }
}

class _$DetailedLinkPageSuccessState implements DetailedLinkPageSuccessState {
  const _$DetailedLinkPageSuccessState(
      {@required this.link, @required this.comments})
      : assert(link != null),
        assert(comments != null);

  @override
  final Link link;
  @override
  final List<Comment> comments;

  @override
  String toString() {
    return 'DetailedLinkPageState.success(link: $link, comments: $comments)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is DetailedLinkPageSuccessState &&
            (identical(other.link, link) ||
                const DeepCollectionEquality().equals(other.link, link)) &&
            (identical(other.comments, comments) ||
                const DeepCollectionEquality()
                    .equals(other.comments, comments)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(link) ^
      const DeepCollectionEquality().hash(comments);

  @override
  $DetailedLinkPageSuccessStateCopyWith<DetailedLinkPageSuccessState>
      get copyWith => _$DetailedLinkPageSuccessStateCopyWithImpl<
          DetailedLinkPageSuccessState>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result loading(Link link),
    @required Result success(Link link, List<Comment> comments),
  }) {
    assert(loading != null);
    assert(success != null);
    return success(link, comments);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result loading(Link link),
    Result success(Link link, List<Comment> comments),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success(link, comments);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result loading(DetailedLinkPageLoadingState value),
    @required Result success(DetailedLinkPageSuccessState value),
  }) {
    assert(loading != null);
    assert(success != null);
    return success(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result loading(DetailedLinkPageLoadingState value),
    Result success(DetailedLinkPageSuccessState value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (success != null) {
      return success(this);
    }
    return orElse();
  }
}

abstract class DetailedLinkPageSuccessState implements DetailedLinkPageState {
  const factory DetailedLinkPageSuccessState(
      {@required Link link,
      @required List<Comment> comments}) = _$DetailedLinkPageSuccessState;

  @override
  Link get link;
  List<Comment> get comments;
  @override
  $DetailedLinkPageSuccessStateCopyWith<DetailedLinkPageSuccessState>
      get copyWith;
}
